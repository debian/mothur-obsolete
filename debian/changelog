mothur (1.39.5-2) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:40:41 +0200

mothur (1.39.5-1) unstable; urgency=medium

  * New upstream version 1.39.5
  * d/patches: refresh

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 25 Mar 2017 12:02:56 +0100

mothur (1.39.4-1) unstable; urgency=medium

  * New upstream version 1.39.4
  * Refresh patches

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 14 Mar 2017 23:28:09 +0100

mothur (1.39.3-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * Corrected EDAM annotation file

  [ Tomasz Buchert ]
  * New upstream version 1.39.3
  * Refresh patches

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 26 Feb 2017 15:56:50 +0100

mothur (1.38.1.1-1) unstable; urgency=medium

  * New upstream version
    Closes: #846486
  * debhelper 10
  * d/watch: version=4
  * Fix spelling

 -- Andreas Tille <tille@debian.org>  Sat, 10 Dec 2016 08:23:57 +0100

mothur (1.38.0-1) unstable; urgency=medium

  * Imported Upstream version 1.38.0

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 30 Jul 2016 01:16:52 +0200

mothur (1.37.6-1) unstable; urgency=medium

  * Imported Upstream version 1.37.6

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 22 Jun 2016 22:15:49 +0200

mothur (1.37.5-1) unstable; urgency=medium

  * Imported Upstream version 1.37.5

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 12 Jun 2016 19:58:02 +0200

mothur (1.37.3-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * better description for mothur-mpi
  * propagate hardening options to uchime build
  * better hardening

 -- Andreas Tille <tille@debian.org>  Sat, 07 May 2016 17:54:19 +0200

mothur (1.36.1-1) unstable; urgency=medium

  * Imported Upstream version 1.36.1
  * d/watch: switch to github & don't repack
  * d/copyright: update and improve
  * d/rules: make build simpler
  * d/patches: refresh and reapply
  * d/control: build with boost library
  * d/control: bump std-ver to 3.9.6
  * d/patches: pass compiler flags properly
  * Added myself as uploader
  * move packaging from SVN to Git

 -- Tomasz Buchert <tomasz@debian.org>  Mon, 07 Sep 2015 10:49:18 +0200

mothur (1.33.3+dfsg-2) unstable; urgency=medium

  * Re-Add changes from BioLinux which reads in SVN:
    Add the important .pat files and tell Mothur how to find them
    (certainly not in /usr/bin) (see debian/patches/find_pat_files)

 -- Andreas Tille <tille@debian.org>  Fri, 25 Apr 2014 17:39:09 +0200

mothur (1.33.3+repack-0biolinux2) trusty; urgency=low

  * Refresh patches and build for Ubuntu Trusty

 -- Tim Booth <tbooth@ceh.ac.uk>  Thu, 24 Apr 2014 18:41:42 +0100

mothur (1.33.3+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 22 Mar 2014 21:33:31 +0100

mothur (1.33.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debian/upstream -> debian/upstream/metadata
  * Standards-Version: 3.9.5

 -- Andreas Tille <tille@debian.org>  Tue, 25 Feb 2014 13:31:27 +0100

mothur (1.31.2+dfsg-2) unstable; urgency=low

  * Team upload.
  * debian/patches/makefile.patch: Leave off -m64, which ia64 doesn't
    support and other 64-bit architectures don't need.  (Closes: #721770.)

 -- Aaron M. Ucko <ucko@debian.org>  Wed, 04 Sep 2013 22:37:31 -0400

mothur (1.31.2+dfsg-1) unstable; urgency=low

  * New upstream version
  * debian/watch: use dversionmangle to properly detect new upstream
    versions
  * debian/copyright: Add Files-Excluded to document what was removed from
    original source
  * debian/rules: Use enhanced uscan
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
     - use anonscm for Vcs fields
  * debian/patches/uchime_hardening.patch: Hardening for uchime
  * debian/uchime.1
  * debian/patches/spelling.patch: Some spelling corrections

 -- Andreas Tille <tille@debian.org>  Tue, 30 Jul 2013 14:32:44 +0200

mothur (1.31.2+repack-0biolinux1) precise; urgency=low

  * New upstream
  * All patches look OK

 -- Tim Booth <tbooth@ceh.ac.uk>  Fri, 16 Aug 2013 12:09:01 +0100

mothur (1.30.2+repack-0ubuntu1) precise; urgency=low

  * New upstream
  * Drop unneeded patch from last time

 -- Tim Booth <tbooth@ceh.ac.uk>  Tue, 30 Apr 2013 11:56:20 +0100

mothur (1.29.0+repack-0ubuntu1) precise; urgency=low

  * New upstream for January
  * Patch to fix compilation on MPI - not sure if it is right

 -- Tim Booth <tbooth@ceh.ac.uk>  Tue, 15 Jan 2013 14:51:19 +0000

mothur (1.28.0+repack-precise2) precise; urgency=low

  * New upstream, build for precise
  * Enable mpi for 32-bit, for completeness
  * Note that this should really be version -0ubuntu2, but its
    too late to go there
  * Parallel build enabled

 -- Tim Booth <tbooth@ceh.ac.uk>  Thu, 10 Jan 2013 15:22:35 +0000

mothur (1.27.0+repack-precise1) precise; urgency=low

  * Remove -march=native from makefile as this makes the result
    depend on the build machine - so a package built on a Core3
    CPU won't work on a Xeon.

 -- Tim Booth <tbooth@ceh.ac.uk>  Fri, 12 Oct 2012 11:41:04 +0100

mothur (1.27.0+repack-precise2) precise; urgency=low

  * New minor release from upstream
  * Build MPI version alongside regular version
  * ...but only for 64-bit

 -- Tim Booth <tbooth@ceh.ac.uk>  Wed, 10 Oct 2012 11:20:43 +0100

mothur (1.25.0~repack-lucid3) lucid; urgency=low

  * Removed dependency on catchall - this belongs in the
    bio-linux-mothur package.  Not sure what I was thinking there.

 -- Tim Booth <tbooth@ceh.ac.uk>  Mon, 14 May 2012 10:57:22 +0100

mothur (1.25.0~repack-lucid2) lucid; urgency=low

  * Fix 32-bit build and add uchime (which should be packaged
    separately, but I've no time for that just now)

 -- Tim Booth <tbooth@ceh.ac.uk>  Thu, 10 May 2012 10:43:04 +0100

mothur (1.25.0~repack-lucid1) lucid; urgency=low

  * Rebuild for Lucid with repack fudge again

 -- Tim Booth <tbooth@ceh.ac.uk>  Wed, 09 May 2012 11:20:15 +0100

mothur (1.25.0-1) UNRELEASED; urgency=low

  * New upstream release
  * debian/upstream: more complete authors information in proper BibTeX
    format

 -- Andreas Tille <tille@debian.org>  Tue, 08 May 2012 18:00:19 +0200

mothur (1.24.1-1) unstable; urgency=low

  * New upstream version
     - adpated patches
     - droped debian/patches/kfreebsd.patch because it was applied
       upstream

 -- Andreas Tille <tille@debian.org>  Tue, 03 Apr 2012 09:05:44 +0200

mothur (1.23.1-1) unstable; urgency=low

  * New upstream version (including updating patches)
  * debian/upstream:
     - fixed typo: s/TITILE/TITLE/
     - Moved DOI+PMID to references
  * debian/control:
     - Standards-Version: 3.9.3 (no changes needed)
     - Build-Depends: gfortran
  * debian/get-orig-source:
     - be not that verbose
     - more reproducible creation of tarball
     - Strip some useless files from tarball (.*; *.o)
  * debhelper 9 (control+compat)
  * debian/copyright: DEP5 changes and verified using
      cme fix dpkg-copyright

 -- Andreas Tille <tille@debian.org>  Thu, 08 Mar 2012 14:52:47 +0100

mothur (1.22.2-2) unstable; urgency=low

  * debian/patches/kfreebsd.patch: Enable build on BSD machines
    (thanks to Christoph Egger <christoph@debian.org> and Robert
    Millan <rmh@debian.org>)
    Closes: #650683
  * debian/control: Fixed Vcs fields
  * debian/{control,rules}: drop explicite quilt dependency which is
    just given by source format 3.0 (quilt)

 -- Andreas Tille <tille@debian.org>  Tue, 06 Dec 2011 17:14:49 +0100

mothur (1.22.2~repack-ubuntu1) lucid; urgency=low

  * Backport for Ubuntu
  * Need to fudge version as upstream was repacked by Andreas'
    script

 -- Tim Booth <tbooth@ceh.ac.uk>  Mon, 05 Dec 2011 17:37:16 +0000

mothur (1.22.2-1) unstable; urgency=low

  * New upstream version
  * debian/patches/drop_sse_option.patch: Fix for FTBFS: unrecognized
    command line option '-msse2' (Thanks for the patch to
    Nobuhiro Iwamatsu <iwamatsu@nigauri.org>)
    Closes: #648255

 -- Andreas Tille <tille@debian.org>  Thu, 10 Nov 2011 08:38:32 +0100

mothur (1.22.1-1) unstable; urgency=low

  [ Thorsten Alteholz ]
  * new upstream version
  * adapted patches to this version
  * debian/control: no dependency on quilt needed
  * debian/control + debian/compat: dh version set to >= 8

  [ Andreas Tille ]
  * debian/control: readd quilt dependency because build fails otherwise
  * debian/copyright: fix spelling error

 -- Andreas Tille <tille@debian.org>  Mon, 07 Nov 2011 11:14:17 +0100

mothur (1.18.1-1) unstable; urgency=low

  * New upstream version (adapted patches to new location of
    makefile)
  * debian/rules: No need to specify SOURCEDIR any more
  * Added Tim Booth <tbooth@ceh.ac.uk> as Uploader,
    removed Sri Girish Srinivasa Murthy <srigirish@evolbio.mpg.de>
    from uploaders
  * Standards-Version: 3.9.2 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Tue, 26 Apr 2011 19:58:45 +0200

mothur (1.18.0-1ubuntu1) lucid; urgency=low

  * Rebuild for PPA
  * Source now comes in subdirectory Mothur.source - modify rules file
    to handle this

 -- Tim Booth <tbooth@ceh.ac.uk>  Thu, 14 Apr 2011 14:50:24 +0100

mothur (1.17.3-1) unstable; urgency=low

  * New upstream version, which specifically solves some copyright
    questions of ftpmaster (Charles, thanks for reviewing
    debian/copyright)
    Closes: #589675
  * Updated patches
  * debian/copyright:
    Clarified license issues

 -- Andreas Tille <tille@debian.org>  Wed, 30 Mar 2011 09:01:52 +0200

mothur (1.15.0-1ubuntu1) lucid; urgency=low

  * Rebuild for upload to PPA, license or not.

 -- Tim Booth <tbooth@ceh.ac.uk>  Sun, 30 Jan 2011 12:14:18 +0000

mothur (1.15.0-1) unstable; urgency=low

  * Initial release (Closes: #589675)

 -- Andreas Tille <tille@debian.org>  Wed, 02 Feb 2011 08:59:45 +0100
